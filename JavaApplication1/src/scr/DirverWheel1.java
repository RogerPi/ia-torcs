package scr;

import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.FunctionBlock;

public class DirverWheel1 extends Controller{
	
        private FIS fis;
        private final String FITXER_FUZZY = "fcl/control.fcl";
        
	/* Gear Changing Constants*/
	final int[]  gearUp={7000,7000,7000,7000,7000,0};
	final int[]  gearDown={0,2500,3000,3000,3500,3000};

	/* Stuck constants*/
	final int  stuckTime = 25;
	final float  stuckAngle = (float) 0.523598775; //PI/6

	/* Accel and Brake Constants*/
	final float maxSpeedDist=70;
	final float maxSpeed=300;
	final float sin5 = (float) 0.08716;
	final float cos5 = (float) 0.99619;

	/* Steering constants*/
	final float steerLock=(float) 0.366519;
	
	/* Clutching Constants */
	final float clutchMax=(float) 0.5;
	final float clutchDelta=(float) 0.05;
	final float clutchRange=(float) 0.82;
	final float clutchDeltaTime=(float) 0.02;
	final float clutchDeltaRaced=10;
	final float clutchDec=(float) 0.01;
	final float clutchMaxModifier=(float) 1.3;
	final float clutchMaxTime=(float) 1.5;
	
	private int stuck=0;

	// current clutch
	private float clutch=0;
	
	public void reset() {
		System.out.println("Restarting the race!");
		
	}

	public void shutdown() {
		System.out.println("Bye bye!");		
	}
	
	
	private int getGear(SensorModel sensors){
	    int gear = sensors.getGear();
	    double rpm  = sensors.getRPM();

	    // if gear is 0 (N) or -1 (R) just return 1 
	    if (gear<1)
	        return 1;
	    // check if the RPM value of car is greater than the one suggested 
	    // to shift up the gear from the current one     
	    if (gear <6 && rpm >= gearUp[gear-1])
	        return gear + 1;
	    else
	    	// check if the RPM value of car is lower than the one suggested 
	    	// to shift down the gear from the current one
	        if (gear > 1 && rpm <= gearDown[gear-1])
	            return gear - 1;
	        else // otherwhise keep current gear
	            return gear;
	}

        public Action control(SensorModel sensors){
		// check if car is currently stuck
		if ( Math.abs(sensors.getAngleToTrackAxis()) > stuckAngle )
	    {
			// update stuck counter
	        stuck++;
	    }
	    else
	    {
	    	// if not stuck reset stuck counter
	        stuck = 0;
	    }
		// after car is stuck for a while apply recovering policy
	    if (stuck > stuckTime)
	    {
	    	/* set gear and sterring command assuming car is 
	    	 * pointing in a direction out of track */
	    	
	    	// to bring car parallel to track axis
	        float steer = (float) (- sensors.getAngleToTrackAxis() / steerLock); 
	        int gear=-1; // gear R
	        
	        // if car is pointing in the correct direction revert gear and steer  
	        if (sensors.getAngleToTrackAxis()*sensors.getTrackPosition()>0)
	        {
	            gear = 1;
	            steer = -steer;
	        }
	        clutch = clutching(sensors, clutch);
	        // build a CarControl variable and return it
	        Action action = new Action ();
	        action.gear = gear;
	        action.steering = steer;
	        action.accelerate = 1.0;
	        action.brake = 0;
	        action.clutch = clutch;
	        return action;
	    }

	    else // car is not stuck
	    {
	        // compute gear 
	        int gear = getGear(sensors);
	        clutch = clutching(sensors, clutch);
	        
                //INICI CONTROLADOR FUZZY
                double angleObjectiu =sensors.getAngleToTrackAxis()-sensors.getTrackPosition()*0.5;
                double distanciaRecorreguda = sensors.getDistanceFromStartLine();
                boolean direccio = (angleObjectiu>0);
                angleObjectiu=Math.abs(angleObjectiu);
                
                double dif = ((double)sensors.getTrackEdgeSensors()[8] - (double)sensors.getTrackEdgeSensors()[9]);
                boolean direccioAngles = (dif>0);

                //BLOC DE GIR
                fis.setVariable("gir", "distanciaRecorreguda", distanciaRecorreguda);
                fis.setVariable("gir", "distanciaFinsCurva", sensors.getTrackEdgeSensors()[9]);
                fis.setVariable("gir", "angleObjectiu", angleObjectiu);
                fis.setVariable("gir","velocitat", sensors.getSpeed());
                FunctionBlock fbgir = fis.getFunctionBlock("gir");
                fbgir.evaluate();
                double gir = fbgir.getVariable("gir").getValue();
                double tipusDireccio = fbgir.getVariable("tipusDireccio").getValue();
                if(tipusDireccio < 0.5)
                    gir = (direccioAngles) ? gir : - gir;
                else
                    gir=(direccio) ? gir : -gir;
                
                //BLOC D'ACCELERACIÓ
                fis.setVariable("acceleracio", "distanciaFinsCurva", sensors.getTrackEdgeSensors()[9]);
                fis.setVariable("acceleracio", "velocitat", sensors.getSpeed());
                FunctionBlock fbacc = fis.getFunctionBlock("acceleracio");
                fbacc.evaluate();
                double acceleracio = fbacc.getVariable("acceleracio").getValue();
 
                //BLOC DE FRE
                fis.setVariable("fre", "distanciaRecorreguda", distanciaRecorreguda);
                fis.setVariable("fre", "distanciaFinsCurva", sensors.getTrackEdgeSensors()[9]);
                fis.setVariable("fre", "velocitat", sensors.getSpeed());
                FunctionBlock fbfre = fis.getFunctionBlock("fre");
                fbfre.evaluate();
                double fre = fbfre.getVariable("fre").getValue();
                
                //RETORNAR L'ACCIÓ
	        Action action = new Action ();
	        action.gear = gear;
	        action.steering = gir;
	        action.accelerate = acceleracio;
	        action.brake = fre;
	        action.clutch = clutch;
	        return action;
	    }
	}

	float clutching(SensorModel sensors, float clutch)
	{
	  	 
	  float maxClutch = clutchMax;

	  // Check if the current situation is the race start
	  if (sensors.getCurrentLapTime()<clutchDeltaTime  && getStage()==Stage.RACE && sensors.getDistanceRaced()<clutchDeltaRaced)
	    clutch = maxClutch;

	  // Adjust the current value of the clutch
	  if(clutch > 0)
	  {
	    double delta = clutchDelta;
	    if (sensors.getGear() < 2)
		{
	      // Apply a stronger clutch output when the gear is one and the race is just started
		  delta /= 2;
	      maxClutch *= clutchMaxModifier;
	      if (sensors.getCurrentLapTime() < clutchMaxTime)
	        clutch = maxClutch;
		}

	    // check clutch is not bigger than maximum values
		clutch = Math.min(maxClutch,clutch);

		// if clutch is not at max value decrease it quite quickly
		if (clutch!=maxClutch)
		{
		  clutch -= delta;
		  clutch = Math.max((float) 0.0,clutch);
		}
		// if clutch is at max value decrease it very slowly
		else
			clutch -= clutchDec;
	  }
	  return clutch;
	}
	
	public float[] initAngles()	{
                fis = FIS.load(FITXER_FUZZY);	
            
		float[] angles = new float[19];
                     
		/* set angles as {-90,-75,-60,-45,-30,-20,-15,-10,-5,0,5,10,15,20,30,45,60,75,90} */
		for (int i=0; i<5; i++)
		{
			angles[i]=-90+i*15;
			angles[18-i]=90-i*15;
		}

		for (int i=5; i<9; i++)
		{
				angles[i]=-20+(i-5)*5;
				angles[18-i]=20-(i-5)*5;
		}
		angles[9]=0;
		return angles;
	}
}
